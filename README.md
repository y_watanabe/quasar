# Installation

## 1. Install Chainer
We must install chainer from forked repository:
https://github.com/ywatanabex/chainer
Select a branch `cmu` and install it.

- First, please **use cudnn v5 or v5.1** before installation.
(v6 does not work with our GRU and LSTM code)
- General Instruction for installing original chainer is found here: https://github.com/pfnet/chainer



## 2. Install additional functions of chainer

Download mychainer repository.
This repository includes additional functions such as attention mechanism.
(this is public repository)

```
$ git clone https://bitbucket.org/y_watanabe/mychainer
```

Add path to this repository like

```
export PYTHONPATH=/home/watay/repos/mychainer:$PYTHONPATH
```

## 3. Get Dataset

Download QUASAR dataset and place it anywhere you like.
if it is like `/home/watay/datasets/quasar` then,
add path like

```export DATAPATH=/home/watay/datasets```


## 4. Work on quasar repository
Download quasar repository.
https://bitbucket.org/y_watanabe/quasar


# About this repository
* quasars: stack-overflow dataset
* quasart: trivia question dataset


# Try

```
$ mv quasars
$ python load.py
$ python train_cmp.py --cl s --model ar0 --gpu 0
```



