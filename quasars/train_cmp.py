"""
Train comprehension model
"""
import os
import pickle
import argparse
import cProfile
import time
import copy
import six
import yaml
import math
from collections import defaultdict

import numpy as np
import chainer
from chainer import cuda
import chainer.functions as F
import chainer.links as L
from chainer import training
from chainer.training import extensions
from chainer import reporter as reporter_module
from chainer import variable
from chainer import link
#from chainer.links.connection.n_step_gru import NStepBiGRU  # DO NOT USE THIS IMPLEMENTATION
from mychainer.links.connection.n_step_bigru import NStepBiGRU
from mychainer.functions.connection.attention_score_dot import attention_score_dot
from mychainer.functions.connection.linear_combination import linear_combination

from load import read_dataset, read_candidates


def concat_lists_with_padding(lists, padding):
    """
    Return n x m shaped list
    n: number of lists
    m: max number of length of lists

    if i-th list is shorter than m, `padding` is padded.
    """
    n = len(lists)
    m = max(map(len, lists))
    ret = []
    for lst in lists:
        ret.append(lst + [padding] * (m - len(lst)))
    return ret


def to_device(batch, device):
    if len(batch) == 0:
        raise ValueError('batch is empty')
    if device is None:
        return batch
    elif device < 0:
        return cuda.to_cpu(batch)
    else:
        return cuda.to_gpu(batch, device)


def crush_vocab0(word):
    """
    Suppose that a word is comes from answer or question sentence.
    The word is often concatenated by hyphen or dot.
    This function split the word into the parts.

    Examples
        'windows-server-2003' -> ['windows', '-' 'server', '-' '2003']
        'developer.uber.com' -> ['developer', '.', 'uver', '.', 'com']
    """
    word = word.replace('-', ' - ')
    word = word.replace('.', ' . ')
    return word.split()


def preprocess_context(sentence):
    """preprocessings for doc sentences
    """
    return sentence.lower()


class Converter(object):
    def __init__(self, candidates, vocab_count_q, vocab_count_c, nc):
        """
        Data converter.

        example of vocab0: 'windows-server-2003'
        example of vocab1: 'windows', '-', 'server', '2003', 'apple', 'clojure', ...
        vocab = vocab0 + vocab1
        """
        self.nc = nc
        #from IPython import embed; embed()
        self.candidates = candidates
        self.n_candidates = len(candidates)
        _candidates_set = set(candidates)
        self.vocab0 = candidates + [w for w in vocab_count_q if w not in _candidates_set]
        _vocab0_set = set(self.vocab0)
        vocab1 = set()
        for word in self.vocab0:
            for w in crush_vocab0(word):
                vocab1.add(w)
        _vocab = self.vocab0 + [w for w in vocab1 if w not in _vocab0_set]
        self.vocab = _vocab + ['SEP', 'UNK']                          # add special tokens
        assert(self.candidates == self.vocab[:len(self.candidates)])  # begins with candidate words

        self._vocab2id = {v: k for (k, v) in enumerate(self.vocab)}
        self.vocabset = set(self.vocab)
        self.n_vocab = len(self.vocab)  # 34418
        def vocab2id(v):
            if v in self.vocabset:
                return self._vocab2id[v]
            else:  # unknown word
                return self.n_vocab - 1
        self.vocab2id = vocab2id

        # candidate features
        #from IPython import embed; embed()
        cand0, cand1 = zip(*[self.answer2feature(cand) for cand in candidates])  # cand0 = range(n_candidates)
        self.cand1 = concat_lists_with_padding(cand1, -1)

    def question2feature(self, q):
        """
        wordids0:  open-source, open-source, open-source
        wordids1:     open,        -,           source
        """
        #from IPython import embed; embed()
        wordids0 = []
        wordids1 = []
        for word0 in q.split():
            wid0 = self.vocab2id(word0)
            wids1 = map(self.vocab2id, crush_vocab0(word0))
            wordids0 += [wid0] * len(wids1)
            wordids1 += wids1
        assert(len(wordids0) == len(wordids1))
        try:
            ind_placeholder = wordids0.index(self.vocab2id('@placeholder'))
        except ValueError:
            ind_placeholder = 0  # for illegal data
        return wordids0, wordids1, ind_placeholder

    def answer2feature(self, a):
        """
        """
        wordid0 = self.vocab2id(a)
        wordids1 = map(self.vocab2id, crush_vocab0(a))
        return wordid0, wordids1

    def context2feature(self, cs):
        if len(cs) == 0:  # no contexts
            c = 'SEP'
        else:
            c = reduce(lambda a, b: a + ' SEP ' + b,  [preprocess_context(ci[1]) for ci in cs[:self.nc]])
        c_wordids0 = []
        c_wordids1 = []
        for word0 in c.split():
            wid0 = self.vocab2id(word0)
            wids1 = map(self.vocab2id, crush_vocab0(word0))
            c_wordids0 += [wid0] * len(wids1)
            c_wordids1 += wids1
        assert(len(c_wordids0) == len(c_wordids1))
        return c_wordids1, c_wordids1

    def softmax_converter(self, batch, device=None):
        #from IPython import embed; embed()
        # contexts
        _c0, _c1 = zip(*[self.context2feature(b['contexts']) for b in batch])
        c0 = [to_device(np.array(c0i, dtype=np.int32), device) for c0i in _c0]
        c1 = [to_device(np.array(c1i, dtype=np.int32), device) for c1i in _c1]

        # question
        _q0, _q1, _q2 = zip(*[self.question2feature(b['question']) for b in batch])
        q0 = [to_device(np.array(q0i, dtype=np.int32), device) for q0i in _q0]
        q1 = [to_device(np.array(q1i, dtype=np.int32), device) for q1i in _q1]
        #q2 = to_device(np.array(_q2, dtype=np.int32), device) # indices of `@placeholser`
        q2 = np.array(_q2, dtype=np.int32)

        # answer
        _a0, _a1 = zip(*[self.answer2feature(b['answer']) for b in batch])
        a0 = to_device(np.array(_a0, dtype=np.int32), device)
        a1 = [to_device(np.array(a1i, dtype=np.int32), device) for a1i in _a1]
        label = a0

        # candidates
        cand0 = to_device(np.array(range(self.n_candidates), dtype=np.int32), device)
        cand1 = to_device(np.array(self.cand1, dtype=np.int32), device)

        return c0, c1, q0, q1, q2, cand0, cand1, label


class AttentionReader0(chainer.Chain):
    """
    One of the simplest model.
    """
    def __init__(self, n_vocab, d_emb, d_gru, n_layers=1, dropout=0.0, train=True):
        super(AttentionReader0, self).__init__(
            # word embedding
            wemb=L.EmbedID(n_vocab, d_emb, ignore_label=-1),
            # question encoder
            qgru=NStepBiGRU(n_layers=1, in_size=d_emb, out_size=d_gru, dropout=dropout),
            # context encoder
            cgru=NStepBiGRU(n_layers=n_layers, in_size=d_emb, out_size=d_gru, dropout=dropout),
            # combination
            Wb0=L.Linear(in_size=2 * d_gru, out_size=d_emb, nobias=True),
            Wb1=L.Linear(in_size=2 * d_gru, out_size=d_emb, nobias=True),
        )
        self.n_vocab = n_vocab
        self.d_emb = d_emb
        self.d_gru = d_gru
        self.train = train

    def __call__(self, context0, context1, question0, question1, candidates0, candidates1):
        batchsize = len(question0)
        xp = cuda.get_array_module(question0[0].data)
        #from IPython import embed; embed()

        # question embedding
        # -------------------
        xs = [self.wemb(q0i) + self.wemb(q1i) for (q0i, q1i) in zip(question0, question1)]
        hy, ys = self.qgru(None, xs, train=self.train)
        q = F.reshape(F.concat([F.expand_dims(hy[0], 2), F.expand_dims(hy[1], 2)], axis=2), (batchsize, 2 * self.d_gru))

        # context embedding
        # -------------------
        us = [self.wemb(c0i) + self.wemb(c1i) for (c0i, c1i) in zip(context0, context1)]
        hv, vs = self.cgru(None, us, train=self.train)

        # attention and output
        # ---------------------
        cs = attention_score_dot(q, vs)
        u0 = linear_combination(vs, cs)
        if not self.train:
            self.attention_score = cs
        u1 = F.reshape(u0, (batchsize, 2 * self.d_gru))

        # score 0
        Went0 = self.wemb(candidates0) + F.sum(self.wemb(candidates1), axis=1)
        out0 = F.matmul(self.Wb0(u1), F.transpose(Went0))
        return out0


class AttentionReader1(chainer.Chain):
    """
    diff from ar0: concatenate embeddings instead of sum
    """
    def __init__(self, n_vocab, d_emb, d_gru, n_layers=1, dropout=0.0, train=True):
        super(AttentionReader1, self).__init__(
            # word embedding
            wemb=L.EmbedID(n_vocab, d_emb, ignore_label=-1),
            # question encoder
            qgru=NStepBiGRU(n_layers=1, in_size=d_emb*2, out_size=d_gru, dropout=dropout),
            # context encoder
            cgru=NStepBiGRU(n_layers=n_layers, in_size=d_emb*2, out_size=d_gru, dropout=dropout),
            # combination
            Wb0=L.Linear(in_size=2 * d_gru, out_size=d_emb, nobias=True),
            Wb1=L.Linear(in_size=2 * d_gru, out_size=d_emb, nobias=True),
        )
        self.n_vocab = n_vocab
        self.d_emb = d_emb
        self.d_gru = d_gru
        self.train = train

    def __call__(self, context0, context1, question0, question1, candidates0, candidates1):
        batchsize = len(question0)
        xp = cuda.get_array_module(question0[0].data)
        #from IPython import embed; embed()

        # question embedding
        # -------------------
        xs = [F.concat([self.wemb(q0i), self.wemb(q1i)], axis=1) for (q0i, q1i) in zip(question0, question1)]
        hy, ys = self.qgru(None, xs, train=self.train)
        q = F.reshape(F.concat([F.expand_dims(hy[0], 2), F.expand_dims(hy[1], 2)], axis=2), (batchsize, 2 * self.d_gru))

        # context embedding
        # -------------------
        us = [F.concat([self.wemb(c0i), self.wemb(c1i)], axis=1) for (c0i, c1i) in zip(context0, context1)]
        hv, vs = self.cgru(None, us, train=self.train)

        # attention and output
        # ---------------------
        cs = attention_score_dot(q, vs)
        u0 = linear_combination(vs, cs)
        if not self.train:
            self.attention_score = cs
        u1 = F.reshape(u0, (batchsize, 2 * self.d_gru))

        # score 0
        Went0 = self.wemb(candidates0) + F.sum(self.wemb(candidates1), axis=1)
        out0 = F.matmul(self.Wb0(u1), F.transpose(Went0))
        return out0


class AttentionReader2(chainer.Chain):
    """
    diff from ar1: use hidden state at `@placeholder` for question encoding
    """
    def __init__(self, n_vocab, d_emb, d_gru, n_layers=1, dropout=0.0, train=True):
        super(AttentionReader2, self).__init__(
            # word embedding
            wemb=L.EmbedID(n_vocab, d_emb, ignore_label=-1),
            # question encoder
            qgru=NStepBiGRU(n_layers=1, in_size=d_emb*2, out_size=d_gru, dropout=dropout),
            # context encoder
            cgru=NStepBiGRU(n_layers=n_layers, in_size=d_emb*2, out_size=d_gru, dropout=dropout),
            # combination
            Wb0=L.Linear(in_size=2 * d_gru, out_size=d_emb, nobias=True),
            Wb1=L.Linear(in_size=2 * d_gru, out_size=d_emb, nobias=True),
        )
        self.n_vocab = n_vocab
        self.d_emb = d_emb
        self.d_gru = d_gru
        self.train = train

    def __call__(self, context0, context1, question0, question1, question2, candidates0, candidates1):
        batchsize = len(question0)
        xp = cuda.get_array_module(question0[0].data)
        #from IPython import embed; embed()

        # question embedding
        # -------------------
        xs = [F.concat([self.wemb(q0i), self.wemb(q1i)], axis=1) for (q0i, q1i) in zip(question0, question1)]
        hy, ys = self.qgru(None, xs, train=self.train)
        q = F.concat([F.expand_dims(ysi[int(n)], 0) for ysi, n in zip(ys, question2.data)], axis=0)

        # context embedding
        # -------------------
        us = [F.concat([self.wemb(c0i), self.wemb(c1i)], axis=1) for (c0i, c1i) in zip(context0, context1)]
        hv, vs = self.cgru(None, us, train=self.train)

        # attention and output
        # ---------------------
        cs = attention_score_dot(q, vs)
        u0 = linear_combination(vs, cs)
        if not self.train:
            self.attention_score = cs
        u1 = F.reshape(u0, (batchsize, 2 * self.d_gru))

        # score 0
        Went0 = self.wemb(candidates0) + F.sum(self.wemb(candidates1), axis=1)
        out0 = F.matmul(self.Wb0(u1), F.transpose(Went0))
        return out0


def get_classifier(classifier_type, predictor, device):
    if classifier_type == 'softmax':
        model =  L.Classifier(predictor, lossfun=F.softmax_cross_entropy, accfun=F.accuracy)
    elif classifier_type == 'sigmoid':
        model =  L.Classifier(predictor, lossfun=F.sigmoid_cross_entropy, accfun=F.binary_accuracy)
    else:
        raise ValueError('Unknown classifier type')

    if device >= 0:
        chainer.cuda.get_device_from_id(device).use()  # Make a specified GPU current
        model.to_gpu()  # Copy the model to the GPU
    return model


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    # data setting
    parser.add_argument('--cl', type=str, choices=['s', 'l'], required=True)  # context length (short or long)
    parser.add_argument('--nc', type=int, default=30)  # context length (short or long)
    # model setting
    parser.add_argument('--model', type=str, choices=['ar0', 'ar1', 'ar2'])
    parser.add_argument('--d_emb', type=int, default=100)  # dim of word embedding
    parser.add_argument('--d_gru', type=int, default=128)  # dim of GRU state
    parser.add_argument('--dropout', type=float, default=0.0)
    # training setting
    parser.add_argument('--resume', type=str, help='resume the training from snapshot')
    parser.add_argument('-g', '--gpu', type=int, default=-1)         # gpu device number (-1 for cpu)
    parser.add_argument('-b', '--batchsize', type=int, default=32)   # batchsize
    parser.add_argument('--epoch', type=int, default=100)  # max epoch
    parser.add_argument('--debug', action='store_true')
    # save
    parser.add_argument('--prefix', type=str)  # add prefix to save_dir
    # evaluation
    parser.add_argument('--eval_mode', type=str)

    # args and save_path
    # -------------------
    args = parser.parse_args()
    context_length = 'short' if args.cl == 's' else 'long'
    batchsize = args.batchsize
    save_path0 = 'results_cmp_{}'.format(context_length)  # quasar-s/short or long

    save_path_keys = ['model', 'd_emb', 'd_gru']
    save_dir = reduce(lambda s1, s2: s1 + ',' + s2, [spk + '=' + str(args.__getattribute__(spk)) for spk in save_path_keys])
    if args.prefix:
        save_dir = args.prefix + ':' + save_dir
    save_path = os.path.join(save_path0, save_dir)
    if not os.path.exists(save_path): os.makedirs(save_path)
    yaml.dump(args, open(os.path.join(save_path, 'args.yml'), 'wt'))

    # load dataset and config
    # ------------------------
    datapath = os.path.join(os.environ['DATAPATH'], 'quasar/quasar-s/')
    print('--- loading dataset ---')
    candidates = read_candidates(datapath)
    dataset = read_dataset(datapath, context_length)
    vocab_count_q = pickle.load(open('vocab_count_q_{}.pkl'.format(args.cl)))
    vocab_count_c = pickle.load(open('vocab_count_c_{}.pkl'.format(args.cl)))
    if args.debug:
        print('  debug mode: use small dataset')
        dataset['train'] = dataset['train'][:200]
        dataset['dev'] = dataset['dev'][:60]
        dataset['test'] = dataset['test'][:60]
    print('--- loading finished ---')


    # data feeding
    # ------------------
    if args.model in ['ar0', 'ar1', 'ar2']:
        converter = Converter(candidates, vocab_count_q, vocab_count_c, args.nc)
        train_converter = converter.softmax_converter
        eval_converter = converter.softmax_converter
    else:
        raise ValueError('Unknown Model')

    # create model
    # -------------------
    if args.model == 'ar0':
        args.cls = 'softmax'
        model = get_classifier(args.cls,
                               AttentionReader0(n_vocab=converter.n_vocab, d_emb=args.d_emb, d_gru=args.d_gru),
                               args.gpu)
        eval_model = model.copy()           # shallow copy
        eval_model.predictor.train = False  # change dropout behavior
    elif args.model == 'ar1':
        args.cls = 'softmax'
        model = get_classifier(args.cls,
                               AttentionReader1(n_vocab=converter.n_vocab, d_emb=args.d_emb, d_gru=args.d_gru),
                               args.gpu)
        eval_model = model.copy()           # shallow copy
        eval_model.predictor.train = False  # change dropout behavior
    elif args.model == 'ar2':
        args.cls = 'softmax'
        model = get_classifier(args.cls,
                               AttentionReader2(n_vocab=converter.n_vocab, d_emb=args.d_emb, d_gru=args.d_gru),
                               args.gpu)
        eval_model = model.copy()           # shallow copy
        eval_model.predictor.train = False  # change dropout behavior
    else:
        raise ValueError('Unknown Model')

    optimizer = chainer.optimizers.Adam()
    optimizer.setup(model)

    # set up dataset
    train_iter = chainer.iterators.SerialIterator(dataset["train"], batch_size=batchsize)
    dev_iter = chainer.iterators.SerialIterator(dataset["dev"], batch_size=batchsize, repeat=False, shuffle=False)
    test_iter = chainer.iterators.SerialIterator(dataset["test"], batch_size=batchsize, repeat=False, shuffle=False)

    updater = training.StandardUpdater(train_iter, optimizer, device=args.gpu, converter=train_converter)
    trainer = training.Trainer(updater, (args.epoch, 'epoch'), out=save_path)

    # evaluations
    trainer.extend(extensions.Evaluator(dev_iter, model, converter=train_converter, device=args.gpu), 'dev0')
    trainer.extend(extensions.Evaluator(dev_iter, eval_model, converter=eval_converter, device=args.gpu), 'dev')
    trainer.extend(extensions.Evaluator(test_iter, eval_model, converter=eval_converter, device=args.gpu), 'test')
    # trainer.extend(extensions.dump_graph('main/loss'))
    trainer.extend(extensions.snapshot(), trigger=(1, 'epoch'))
    trainer.extend(extensions.LogReport())
    trainer.extend(extensions.PrintReport(
        ['epoch', 'main/loss',  'main/accuracy',
         'dev0/main/loss', 'dev0/main/accuracy',
         'dev/main/accuracy', 'test/main/accuracy']))
    trainer.extend(extensions.ProgressBar())

    if not args.eval_mode:
        print('start training!!')
        trainer.run()