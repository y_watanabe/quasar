# -*- coding: utf-8 -*-
"""
Load QUSAR-S dataset
"""
import os
import re
import pickle
import collections
import gzip
import argparse

import numpy as np


def read_data(path):
    with gzip.open(path) as f:
        for line in f:
            yield eval(line)


def read_dataset(datapath, contexts_length):
    dataset = {}
    for splt in ['train', 'dev', 'test']:
        contexts = '{}_contexts.json.gz'.format(splt)
        questions = '{}_questions.json.gz'.format(splt)
        dataset[splt] = list(read_data(os.path.join(datapath, contexts_length, questions)))
        for c, data in zip(read_data(os.path.join(datapath, contexts_length, contexts)), dataset[splt]):
            data['contexts'] = c
    return dataset


def read_candidates(datapath):
    with open(os.path.join(datapath, 'candidates.txt')) as f:
        candidates = [word.strip() for word in f.readlines()]
    return candidates

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--cl', type=str, choices=['s', 'l'], required=True)  # context length = short or long
    parser.add_argument('--stats', action='store_true')
    args = parser.parse_args()
    context_length = 'short' if args.cl == 's' else 'long'

    datapath = os.path.join(os.environ['DATAPATH'], 'quasar/quasar-s/')
    # load candidates
    candidates = read_candidates(datapath)
    print('Number of candidates = {}'.format(len(candidates)))

    print('')
    print('')
    print('======================================================')
    print('{} dataset'.format(context_length))
    print('======================================================')
    dataset = read_dataset(datapath, context_length)

    if args.stats:
        print('Basic statistics')
        print('----------------')
        # number of samples
        for splt in ['train', 'dev', 'test']:
            print('Number of data in {} dataset = {}'.format(splt, len(dataset[splt])))

        # question lengths
        #for splt in ['train', 'dev', 'test']:
        for splt in ['train']:
            question_lens = [len(data['question'].split()) for data in dataset[splt]]
            print('Question Length mean = {}'.format(np.mean(question_lens)))
            print('Question Length stddev = {}'.format(np.sqrt(np.var(question_lens))))
            print('Question Length max = {}'.format(np.max(question_lens)))

        # contexts lengths
        #for splt in ['train', 'dev', 'test']:
        for splt in ['train']:
            contexts_lens = [[len(c[1].split()) for c in data['contexts']] for data in dataset[splt]]
            print('Number of Contexts: mean = {}'.format(np.mean(map(len, contexts_lens))))
            print('Number of Contexts: max = {}'.format(np.max(map(len, contexts_lens))))
            print('Number of Contexts: min = {}'.format(np.min(map(len, contexts_lens))))
            print('Number of Empty Contexts = {}'.format(len([c for c in contexts_lens if len(c) == 0])))
            print('Number of Words in a Context: mean = {}'.format(
                np.mean([0 if len(c)==0 else np.mean(c) for c in contexts_lens])))

    print('')
    print('Vocabulary')
    print('----------')
    vocab_count_a = collections.defaultdict(int)
    vocab_count_q = collections.defaultdict(int)
    vocab_count_c = collections.defaultdict(int)
    for splt in ['train', 'dev', 'test']:
        for data in dataset[splt]:
            vocab_count_a[data['answer']] += 1

            for word in data['question'].split():
                vocab_count_q[word] += 1

            for context in data['contexts']:
                _, doc = context
                for word in doc.split():
                    vocab_count_c[word] += 1

    print('Vocabulary size of answers = {}'.format(len(vocab_count_a)))  # same as len(candidates)
    print('Vocabulary size of questions = {}'.format(len(vocab_count_q)))
    print('Vocabulary size of contexts = {}'.format(len(vocab_count_c)))
    pickle.dump(vocab_count_q, open('vocab_count_q_{}.pkl'.format(args.cl), 'wb'))
    pickle.dump(vocab_count_c, open('vocab_count_c_{}.pkl'.format(args.cl), 'wb'))

    if args.stats:
        for n in [5, 10]:
            print('Ration of words less than {} counts'.format(n))
            print('  vocab anser: {}'.format(1.0 * len([w for w in vocab_count_a if vocab_count_a[w] < n]) / len(vocab_count_a)))
            print('  vocab question: {}'.format(1.0 * len([w for w in vocab_count_q if vocab_count_q[w] < n]) / len(vocab_count_q)))
            print('  vocab contexts: {}'.format(1.0 * len([w for w in vocab_count_c if vocab_count_c[w] < n]) / len(vocab_count_c)))







